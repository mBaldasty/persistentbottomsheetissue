import Vue from 'nativescript-vue'
import Home from './components/Home'
import BottomSheetPlugin from '@nativescript-community/ui-persistent-bottomsheet/vue';
import { install } from '@nativescript-community/ui-persistent-bottomsheet';
install();
Vue.use(BottomSheetPlugin);
Vue.config.silent = false
new Vue({
  render: (h) => h('frame', [h(Home)]),
}).$start()
